#!/bin/bash
if [ $# -lt 1 ]; then
echo "no se introdujeron todos los argumentos."
exit 1
fi
AYUDA="C. Fran Torres Gallego, ASIX 2020.\nEste programa muestra el número de días en un mes dado."
mes=$1
if [ ! $mes -ge 1 -a $mes -le 12 ]; then
echo "los meses son entre 1 y 12."
exit 2
fi
case $mes in
1|3|5|7|8|10|12)
echo "tiene 31 días"
;;
4|6|9|11)
echo "tiene 30 días"
;;
2)
echo "el único margi con 28"
;;
*)
echo "tu eres tonto. Adiós"
;;
esac
case $mes in
-h|--help)
echo $AYUDA
;;
esac
