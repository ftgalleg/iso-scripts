#!/bin/bash
if [ $# -lt 1 ]; then
echo "error, no args input. a number between 0 and 10 is required"
exit 1
fi
num=$1
if [ $num -gt 0 -a $num -lt 5 ]; then
echo "suspent"
elif [ $num -ge 5 -a $num -le 10 ]; then
echo "aprobed"
else
echo "no valid number. You should enter value from 0 to 10"
fi

