#!/bin/bash
if [ $# -lt 1 ]; then
echo "no args input. You should enter a valid filename or directory"
exit 1
fi
fichero="$1"
if [ -d $fichero ]; then
echo "$fichero. Is a directory"
elif [ -L $fichero ]; then
echo "$fichero. Is a symlink"
elif [ -f $fichero ]; then
echo "$fichero. Is a regular file"
elif [ -s $fichero ]; then
echo "$fichero. Is a shocket"
elif [ -x $fichero ];then
echo "$fichero. Is a executable file"
elif [ -c $fichero ]; then
echo "$fichero. Is a character device"
elif [ -b $fichero ]; then
echo "$fichero. Is a block device"
else
echo "$fichero. Isn't valid filename"
exit 2
fi

