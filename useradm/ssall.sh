#!/bin/bash
function ShowShadow(){
if [ $# -lt 1 ]; then
echo "error, se esperaba almenos un argumento"
return 1
fi
login=$1
info=$(grep "^$login:" /etc/shadow)
if [ $? -ne 0 ]; then
echo "error, el login introducido no existe"
return 2
fi
lname=$(echo $info | cut -d ':' -f1)
fechacambio=$(echo $info | cut -d ':' -f3)
min=$(echo $info | cut -d ':' -f4)
max=$(echo $info | cut -d ':' -f5)
warn=$(echo $info | cut -d':' -f6)
inactive=$(echo $info | cut -d ':' -f7)
expire=$(echo $info | cut -d':' -f8)
echo "nombre: $lname"
echo "Fecha del ultimo cambio: $fechacambio"
echo "tiempo minimo (en años): $min"
echo "tiempo maximo (en años): $max"
echo "tiempo para advertir: $warn"
echo "tiempo de inactividad: $inactive"
echo "tiempo de expiracion: $expire"
return 0
}
function ShowShadowList(){
if [ $# -lt 1 ]; then
echo "error, se esperaba almenos un argumento"
return 1
fi
for logins in $*
do
info=$(grep "^$logins:" /etc/shadow)
if [ $? -ne 0 ]; then
echo "error, el usuario indicado no existe" >> /dev/stderr
fi
lname=$(echo $info | cut -d ':' -f1)
fechacambio=$(echo $info | cut -d ':' -f3)
min=$(echo $info | cut -d ':' -f4)
max=$(echo $info | cut -d ':' -f5)
warn=$(echo $info | cut -d':' -f6)
inactive=$(echo $info | cut -d ':' -f7)
expire=$(echo $info | cut -d':' -f8)
echo "nombre: $lname"
echo "Fecha del ultimo cambio: $fechacambio"
echo "tiempo minimo (en años): $min"
echo "tiempo maximo (en años): $max"
echo "tiempo para advertir: $warn"
echo "tiempo de inactividad: $inactive"
echo "tiempo de expiracion: $expire"
done
return 0
}

function ShowShadowIn(){
contar=0
while read -r login
do
info=$(grep "^$login:" /etc/shadow)
if [ $? -ne 0 ]; then
echo "error, el usuario indicado no existe" >> /dev/stderr
fi
lname=$(echo $info | cut -d ':' -f1)
fechacambio=$(echo $info | cut -d ':' -f3)
min=$(echo $info | cut -d ':' -f4)
max=$(echo $info | cut -d ':' -f5)
warn=$(echo $info | cut -d':' -f6)
inactive=$(echo $info | cut -d ':' -f7)
expire=$(echo $info | cut -d':' -f8)
echo "nombre: $lname"
echo "Fecha del ultimo cambio: $fechacambio"
echo "tiempo minimo (en años): $min"
echo "tiempo maximo (en años): $max"
echo "tiempo para advertir: $warn"
echo "tiempo de inactividad: $inactive"
echo "tiempo de expiracion: $expire"
contar=$(($contar+1))
done
return 0
}
