#!/bin/bash
if [ $# -lt 2 ]; then
echo "error, se esperaban dos argumentos"
exit 1
fi
grupoclase=$1
curso=$2
addgroup $grupoclase
if [ $? -ne 0 ]; then
echo "el grupo ya existe"
fi
for clase in $curso
do
adduser -G $curso -g 100 -b /home/$curso -d $clase -m $clase-{01-30}
usermod -p "alumn" $clase-{01-30}
done
