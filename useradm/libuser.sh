#!/bin/bash
function sugcos() {
if [ $# -lt 1 ]; then
echo "se esperaba almenos un argumento" >> /dev/stderr
return 1
fi
usuario="$1"
grep "^$usuario$" /etc/passwd
if [ $? -eq 0 ]; then
info=$(egrep "^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:$" /etc/passwd)
gcos=$(echo $info | cut -d ':' -f 5)
nombre=$(echo $gcos | cut -d ',' -f1)
sala=$(echo $gcos | cut -d ',' -f2)
tel=$(echo $gcos | cut -d ',' -f3)
echo -e "La información del usuario es: \n Nombre: $nombre. \n Habitación: $sala. \n Teléfono: $tel"
return 0
fi
}
function sgroup(){
if [ $# -lt 1 ]; then
echo "se esperaba almenos un argumento" 2>> /dev/null
return 1
fi
gid=$1
usuario=$(grep "^[^:]*:[^:]*:[^:]*:$gid:$" /etc/passwd)
grupo=$(grep "^[^:]*:[^:]*:$gid:$" /etc/group)

login=$(echo $usuario | cut -d ':' -f1)
uid=$(echo $usuario | cut -d ':' -f3)
homedir=$(echo $usuario | cut -d ':' -f6)
shell=$(echo $usuario | cut -d ':' -f7)
gname=$(echo $grupo | cut -d ':' -f1)
echo "nombre: $login"
echo "uid: $uid"
echo "Home dir: $homedir"
echo "shell: $shell"
echo "group name: $gname"
}
function UserList(){
if [ $# -lt 1 ]; then
echo "se esperaba uno o más argumentos" >> /dev/stderr
return 1
fi
while read -r line
do
for usuarios in $line
do
grep "^$usuarios:$" /etc/passwd
if [ $? -eq 0 ]; then
echo $usuarios
else
echo $usuarios >> /dev/stderr
fi
done
done
return 0
}
