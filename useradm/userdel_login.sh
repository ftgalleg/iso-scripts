#!/bin/bash
if [ $# -lt 1 ]; then
echo "error, se esperaba almenos un argumento"
exit 1
fi
for login in $*
do
tar cvzf /root/backup/$login.tar.gz $(find / -user $login)
if [ $? -ge 0 ]; then
deluser -r $login >> /dev/stderr
else
echo "error, no se pudo borrar el usuario $login"
fi
done
exit $?

