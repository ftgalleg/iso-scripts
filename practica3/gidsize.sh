#!/bin/bash
# Fran Torres Gallego
# isx: 46405805
# https://gitlab.com/ftgalleg/iso-scripts
#04/2020
# ASIX-ISO-2019-2020
function gidsize(){
gid=$1
if [ $# -lt 1 ]; then
echo "no se ha introducido ningun gid"
return 1
fi
grep "^[^:]*:[^:]*:$gid:$" /etc/group
if [ $? -ne 0 ]; then
echo "error, el gid no existe"
return 2
fi
login=$(grep $gid /etc/group)
group=$(cut -d ':' -f4 $login)
#se da por hecho que el home se encuentra dentro del directorio raíz /home.
users=$(echo $group | cut -d ',' -f 1)
for sizes in $users
do
du -h /home/$sizes
done
return 0
}
