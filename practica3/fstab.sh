#!/bin/bash
# Fran Torres Gallego
# isx: 46405805
# https://gitlab.com/ftgalleg/iso-scripts
#04/2020
# ASIX-ISO-2019-2020
function fstype(){
if [ $# -lt 1 ]; then
echo "no se introdujo ningun sistemas 
return 1
fi
file=$1
normal=$(grep -v '^#' /etc/fstab)
fs=$(echo $normal | tr '[:blank:]' ' ')
filetype=$(grep file $fs)
device=$(echo $filetype | cut -d ' ' -f1)
mp=$(echo $filetype | cut -d ' ' -f2)
echo "dispositivo: $device"
echo "punto de montaje: $mp"
return 0
}