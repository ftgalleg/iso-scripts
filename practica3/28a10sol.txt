1) Disc virtual + format
    Crear un disc virtual de 100M de tipus Vfat, nom Disc1.img.
    Assignar-li etiqueta disc-dos.
    Muntar/desmuntar a mnt.

dd if=/dev/zero of=Disc1.img bs=1024 count=100K
losetup /dev/loop0 Disc1.img; mkfs -t vfat Disc1.img
doslabel /dev/loop0 disc2
mount -t vfat Disc1.img /mnt/; umount /mnt

2) Disc virtual + format
    Crear un disc virtual de 200M de tipus ext3, nom Disc2.img.
    Assignar-li etiqueta linux-prova.
    Muntar/desmuntar a mnt.

dd if=/dev/zero of=Disc2.img bs=1024 count=200K
losetup /dev/loop1 Disc2.img; mkfs -t ext3 Disc2.img
e2label /dev/loop1 linux-prova
mount -t ext3 Disc2.img /mnt/; umount /mnt

3) Disc virtual + format
    Crear un disc virtual de 300M de tipus ntfs, nom Disc3.img.
    Assignar-li etiqueta win-disc.
    Muntar/desmuntar a mnt.

dd if=/dev/zero of=Disc3.img bs=1024 count=300K
losetup /dev/loop2 Disc3.img; mkfs -t ntfs Disc3.img
ntfslabel /dev/loop2 win-disc
mount -t ntfs Disc3.img /mnt/; umount /mnt

4) Identificar ordres i paquets
    Llista totes les ordres que contenen la cadena label
    Llista totes les ordres que tenen la cadena ntfs
    Identifica el paquet al que pertany la ordre ntfslabel

which -a label
which -a ntfs
dpkg -l | grep '^ntfslabel'
-- Mount manuals--------------------------------------------------------

5) Mount manual
    Muntar la partició de la tarda a /mnt
    Tornar-la a muntar (en calent) ara només coma a ro.

mont -t ext4 /dev/sda6 /mnt
mount -o remount,ro -t ext4 /dev/sda6 /mnt
6) Els devices loop
    Muntar usant losetup el Disc1.img a /tmp/d1
    Muntar directament usant únicament mount el Disc2.img a /tmp/d2
losetup /dev/loop0 disc1.img; mount -t vfat disc1.img /mnt
mount -t ext3 -o loop disc2.img /mnt

7) Imatges iso
    Localitza una imatge .iso (a gandhi).
    Muntar-la a /tmp/iso. navegar per el seu contingut. Fer-ne un tree.
mount -t iso9660 -o loop debian-11.0.0-amd64-DVD-1.iso /mnt
tree /mnt
-- Fstab ---------------------------------------------------------------

8) Label i uuid
    Definir el muntatge automàtic de Disc1.img a /tmp/d1 per label
    Definir el muntatge automàtic de Disc2.img a /tmp/d2 per uuid
    Definir el muntatge automàtic de Disc3.img a /tmp/d3 però que no 
    es munti automàticament sinó que qualsevol usuari ho pugui fer quan 
    vulgui.
    Cpmprovar-ho rebotant el sistema!
LABEL=disc2 /mnt vfat defaults 0 0
LABEL=win-disc ntfs /mnt users,noauto 0 0
9) Imatges iso (per ftp a gandhi en trobareu a pub/images)
    Definir el muntatge automatitzat d'una imatge iso a /tmp/iso de 
    manera que qualsevol usuari pugui muntar i qualsevol altre usuari
    pugui desmuntar.
/dev/sr0 /tmp/iso iso9660 users,noauto 0 0 
-- Fsck ---------------------------------------------------------------

10) Xequeig de particions
    fer un xequeig de la partició de la tarda
    fer un quequeig de cada un dels disc virtuals (Disc1.img, Disc2.img 
    i Disc3.img).

fsck -y /dev/sda6
fsck -y disc1.img
fsck -y disc2.img
fsck -y disc3.img

-- Swap ---------------------------------------------------------------

11) Activar/Desactivar, Llistar swap
    Llistar els swaps actius.
    Desactivar el swap manualment.
swapon -s
swapoff /dev/sda7
12) Swap usant el loop
    Assignar a Disc2.img el format swap usant el deu device de loop
    Activar manaulment aquest swap. llistar els swaps.
mkswap -f /dev/loop9
swapon /dev/loop9

13) Swap usant directament un fitxer qualsevol.
    Assignar al fitxer Disc3.img el format swap (directament al fitxer)
    Activar manualment el swap. Llistar els Swap.
    Desactivar tots els swap.
swapoff -a
mkswap -f disc3.img
swapon disc3.img

14) Activar els swap automàticament
    Configurar fstab per tal d'utilitzar els tres swaps. 
    Comprovar-ho arrancant de nou!.
    Restaura els swaps a la configuració per defecte de l'aula.

-- df, du, lsof --------------------------------------------------------

15) Anàlisi de l'espai dels devices
    * Llistar l'espai total i el disponible de cada partició muntada, tot
    mostrant el sistema de fitxers que utilitza. Cal mostrar la info en 
    unitas K, M o G.
df -hT 
    * Llistar només els sistemes de fitxers ext3.
df -hT | grep ext3
    * Llistar només els sistemes de fitxers virtuals. Quins són?
    * Llistar l'ocupació d'inodes només dels sistemes locals.
    df -lih

16) Llistat de l'ocupació de l'espai de disc.
    - Llistar (K, M o G) l'ocupació de disc de /boot. 
du -h /boot
    - Llistar l'ocupació de disc de /etc resumint a un nivell de 
    profunditat.
du -h /etc --max-depth=1
    - Llistar la ocupació de /etc, /boot i /tmp sumaritzant, però sense
    incloure en el còmput els fitxers .img.
du -sh /boot /etc /tmp --exclude=*.img
17) Detectar fitxers oberts
    Llistar quins fitxers tenim oberts en la partició matí
    Muntar la partició de la tarda a /mnt. Fer actiu aquest directori
    i llistar els fitxers oberts d'aquesta partició.
lsof /dev/sda5
mount -t ext4 /dev/sda6 /mnt; lsof /mnt
-- Altres ordres -------------------------------------------------------

18) Ordre stat
    Aplica l'ordre stat a un fitxer, a un directori, a un link a cada un
    dels discs virtuals que hem creat.
    Aplicala també a /dev/sda6.
stat /boot
stat /home/ftorres/asix/sis/uf2/scripts/iso-scripts/useradm/ShowUser.sh
stat disc1.img
stat /dev/sda6
stat /vmlinuz.old
19) Ordres vàries.
    - Prova les ordres: free, vmstat, top, uptime, hdparam.
    - Llistar la informació que descriu l'estructura del sistema de 
    fitxers d'una partició (blocs, inodes, features, mount count, etc).
    - Assignar a /dev/sda6 que cada 80 muntatges o cada 130 dies es faci
    el xequeig automatitzat. Comprovar aquests canvis.

20) Enllaços durs i enllaços simbòlics.
    - Crear un fitxer, un enllaç dur a aquest fitxer i un de simbòlic.
    - Llistar els inodes dels tres fitxers. Fer l'ordre stat a tots tres.
    - Usar find per localitzar tots els fitxers que corresponen a un 
    mateix hard link.
    - Usar find per localitzar tots els fitxers que corresponen a un 
    mateix enllaç simbòlic.

