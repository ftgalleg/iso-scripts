#!/bin/bash
# Fran Torres Gallego
# isx: 46405805
# https://gitlab.com/ftgalleg/iso-scripts
#04/2020
# ASIX-ISO-2019-2020
function fsize(){
for login in $*
do
grep $login /etc/passwd
if [ $? -ne 0 ]; then
echo "el usuario no existe" >> /dev/stderr
fi
home=$(cut -d ':' -f 6 /etc/passwd)
du -h $home
done
return 0
}
function loginfile(){
if [ ! -f /dev/stdin ]; then
echo "se esperaba almenos un fichero de logins"
return 1
fi
contar=0
while read -r line
do
home=$(cut -d ':' -f 6 /dev/passwd)
du -h $home
done < /dev/stdin
return 0
}
function grepgid(){
if [ $# -lt 1 ]; then
echo "error, se esperaba almenos un gid"
return 1
fi
gid=$1
grep "^[^:]*:[^:]*:$gid:$" /etc/group
if [ $? -ne 0 ]; then
echo "error, el gid introducido no existe o no es valido" >> /dev/stderr
return 2
fi
login=$(grep "^[^:]*:[^:]*:[^:]*:$gid:$" /etc/passwd)
echo $login | cut -d ':' -f 1)
return 0
}
