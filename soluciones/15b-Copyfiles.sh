#! /bin/bash 
# Alejandro Gambín
# Febrer 2020
# Hacer un cp de varios regulars files a un directorio 
# ------------------------------------------
OK=0
ERR_NARGS=1
# Validació #args 

file=$1
dir=$2
#if ! [ -e file -a -e dir ];then
#        echo "Error: Alguno de los valores no existe "
#        echo "Usage: $0 file dir"
#        exit $ERR_NOEXIST
#fi
if [ $# -le 2 ];then
	echo "Error: nº args incorrecte"
	echo "Usage: $0 file[...] dir-destí"
	exit $ERR_NARGS
fi

desti=$($* | sed 's/^.* //' )
llistaFile=$($* | sed 's/ [^ ]*$//')



for file in $llistaFile
do
	if [ ! -f $file ]; then
		echo "Error: $file no es un regular file " >> /dev/stderr 
		echo "Usage: $0 regularFile[...] dir-destí" >> /dev/stder
	fi

	cp $file $dir 
done
