#! /bin/bash 
# Alejandro Gambín
# Febrer 2020
# Diu quines son les opcions y els arguments 
# --------------------------------------------
OK=0
ERR_NARGS=1


# Validació #args 

# Separar llistas una será opcions i l'altre arguments

# Cualsevol cosa que no sigui una opció es un argument

if [ $# -eq 0 ];then
	echo "Error: Nº args incorrecte"
	echo "Usage: $0 [ -a -b -c -d -e -f ] args[...]"
	exit $ERR_NARGS
fi

opcions=""
arguments=""

for arg in $*
do
  case $arg in
	  -[a-f])
	opcions="$opcions $arg";;
	*)	
	arguments="$arguments $arg";;
  esac 

done

echo "Opcions: $opcions"
echo "Arguments: $arguments"
