#! /bin/bash 
# Alejandro Gambín
# Febrer 2020
# Fer un cp d'un regular file a un dir 
# ------------------------------------
OK=0
ERR_NARGS=1
ERR_NOFILE=2
ERR_NODIR=3
# Validació #args 

if [ $# -ne 2 ];then
        echo "Error: #args incorrecte"
        echo "Usage: $0 file dir"
        exit $ERR_NARGS
fi


if ! [ -f $1 ];then
    echo "Error: $file no es un regular file"
    echo "Usage: $0 file dir"
    exit $ERR_NOFILE
fi

if ! [ -d $2 ];then
    echo "Error: $dir no es un directori"
    echo "Usage: $0 file dir"
    exit $ERR_NODIR
fi

cp $1 $2

