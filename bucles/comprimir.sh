#!/bin/bash
if [ $# -lt 1 ]; then
echo "no se ha introducido ningún argumento." >> /dev/stderr
exit 1
fi
contar=0
for comprimir in $*
do
tar cvzf $comprimir.tar.gz $comprimir > /dev/null
if [ $? -eq 0 ]; then
echo $comprimir
contar=$(($contar+1))
else
echo "no se ha podido comprimir $comprimir" > /dev/stderr
exit 2
fi
done
echo "se han comprimido $contar; ficheros correctamente"
exit 0
