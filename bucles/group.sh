#!/bin/bash
if [ $# -lt 1 ]; then
echo "no se han introducido argumentos" > /dev/stderr
exit 1
fi
gid=$1
for buscar in $gid
do
gname=$(grep "^[^:]*:[^:]*:$buscar:$ /etc/group | cut -d ':' -f 1)
idgrp=$(grep "^[:]*:[^:]*:$buscar:$" /etc/group | cut -d ':' -f 3 /etc/group)
users=$(grep "^[^:]*:[^:]*:$buscar:$" /etc/group | cut -d ':' -f 4)
echo "nombre de grupo: $gname. Identificador: $idgrp. Usuarios: $users"
done
