#!/bin/bash
if [ $# -lt 1 ]; then
echo "no args input. You should type a digit between 0 to 10"
exit 1
fi
for num in $*
do
if [ $num -ge 0 -a $num -le 4 ]; then
echo "suspent"
elif [ $num -ge 4 -a $num -le 6 ]; then
echo "aprobado"
elif [ $num -ge 7 -a $num -le 8 ]; then
echo "notable"
elif [ $num -ge 9 -a $num -le 10 ]; then
echo "excellent"
else
echo "the number isn't valid. The valid range is from 0 to 10."
fi
done
