#!/bin/bash
if [ $# -lt 1 ]; then
echo "error, no se han introducido argumentos" > /dev/stderr
elif [ $# -lt 4 ]; then
echo "error, faltan argumentos" > /dev/stderr
exit 1
fi
for tipos in $*
do
if [ -f $tipos ]; then
ls -l $tipos > /dev/null
echo "$tipos los 4 son ficheros"
elif [ -d $tipos ]; then
ls -d $tipos > /dev/null
echo "$tipos los 4 son directorios"
exit 0
else
echo "$tipos no es ni ficheros ni directorios" >> /dev/stderr
exit 2
fi
done
