#!/bin/bash
novalidas=0
validas='^[0-9]{4}-[A-Z]{4}$'
for matriculas in $*
do
echo $matriculas | egrep $validas &> /dev/null
if [ $? -ne 0 ]; then
novalidas=$(($novalidas+1))
echo $matriculas >> /dev/stderr
else
echo $matriculas
fi
done
exit $novalidas
