#!/bin/bash
for uid in $*
do
usuario=$(grep "^[^:]*:[^:]*:$uid:$" /etc/passwd)
if [ $? -eq 0 ]; then
login=$(cut -d ':' -f 1 $usuario)
gname=$(grep "^[^:]*:[^:]*:$1:$" /etc/group | cut -d ':' -f1)
if [ $? -eq 0 ]; then
grupo=$(cut -d ':' -f1 $gname)
echo "$login, $grupo"
fi
fi
done


