#!/bin/bash
if [ $# -lt 1 ]; then
echo "error, no argument's found"
exit 1
fi
c=0
listdir="$1"
if [ -d $listdir ]; then
for lista in ls -d $listdir
do
c=$(($c+1))
echo "$c, $lista"
done
else
echo "error, is not a directory"
exit 2
fi
