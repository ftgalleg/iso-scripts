#! /bin/bash
# @edt ASIX-M01 Curs 2019-2020
# febrer 2020
# Descripcio: Exercici10.sh
#             Fer un programa que rep com a argument un número indicatiu del 
#             número màxim de línies a mostrar. El programa processa stdin 
#             línia a línia i mostra numerades un màxim de num línies.
# -----------------------------------------------------------------------------------
max_linies=$1
comptador=1
while read -r line
do
	if [ $max_linies -ge $comptador ]
	then
		echo "$comptador $line"
		comptador=$((comptador+1))
	else
		exit 0
	fi
done

