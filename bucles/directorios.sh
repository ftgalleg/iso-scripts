#!/bin/bash
if [ $# -lt 1 ]; then
exit 1
fi
dir=""
while [ -n "$1" ]
do
dir="$dir $1"
mkdir $dir &> /dev/null
if [ $? -ne 0 ]; then
echo "error, el directorio $dir no se pudo crear" 2> /dev/stderr
shift
fi
done
echo "directorios creados con éxito"
exit 0
